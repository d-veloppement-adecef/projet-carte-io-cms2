#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteIO.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteIO.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../project/GenericTCPClient.c ../project/GenericTCPServer.c ../project/ARP.c ../project/DNS.c ../project/ETH97J60.c ../project/Helpers.c ../project/ICMP.c ../project/IP.c ../project/StackTsk.c ../project/TCP.c ../project/Tick.c ../project/UDP.c ../project/main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o ${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o ${OBJECTDIR}/_ext/1989816886/ARP.o ${OBJECTDIR}/_ext/1989816886/DNS.o ${OBJECTDIR}/_ext/1989816886/ETH97J60.o ${OBJECTDIR}/_ext/1989816886/Helpers.o ${OBJECTDIR}/_ext/1989816886/ICMP.o ${OBJECTDIR}/_ext/1989816886/IP.o ${OBJECTDIR}/_ext/1989816886/StackTsk.o ${OBJECTDIR}/_ext/1989816886/TCP.o ${OBJECTDIR}/_ext/1989816886/Tick.o ${OBJECTDIR}/_ext/1989816886/UDP.o ${OBJECTDIR}/_ext/1989816886/main.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o.d ${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o.d ${OBJECTDIR}/_ext/1989816886/ARP.o.d ${OBJECTDIR}/_ext/1989816886/DNS.o.d ${OBJECTDIR}/_ext/1989816886/ETH97J60.o.d ${OBJECTDIR}/_ext/1989816886/Helpers.o.d ${OBJECTDIR}/_ext/1989816886/ICMP.o.d ${OBJECTDIR}/_ext/1989816886/IP.o.d ${OBJECTDIR}/_ext/1989816886/StackTsk.o.d ${OBJECTDIR}/_ext/1989816886/TCP.o.d ${OBJECTDIR}/_ext/1989816886/Tick.o.d ${OBJECTDIR}/_ext/1989816886/UDP.o.d ${OBJECTDIR}/_ext/1989816886/main.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o ${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o ${OBJECTDIR}/_ext/1989816886/ARP.o ${OBJECTDIR}/_ext/1989816886/DNS.o ${OBJECTDIR}/_ext/1989816886/ETH97J60.o ${OBJECTDIR}/_ext/1989816886/Helpers.o ${OBJECTDIR}/_ext/1989816886/ICMP.o ${OBJECTDIR}/_ext/1989816886/IP.o ${OBJECTDIR}/_ext/1989816886/StackTsk.o ${OBJECTDIR}/_ext/1989816886/TCP.o ${OBJECTDIR}/_ext/1989816886/Tick.o ${OBJECTDIR}/_ext/1989816886/UDP.o ${OBJECTDIR}/_ext/1989816886/main.o

# Source Files
SOURCEFILES=../project/GenericTCPClient.c ../project/GenericTCPServer.c ../project/ARP.c ../project/DNS.c ../project/ETH97J60.c ../project/Helpers.c ../project/ICMP.c ../project/IP.c ../project/StackTsk.c ../project/TCP.c ../project/Tick.c ../project/UDP.c ../project/main.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteIO.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F97J60
MP_PROCESSOR_OPTION_LD=18f97j60
MP_LINKER_DEBUG_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o: ../project/GenericTCPClient.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o   ../project/GenericTCPClient.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o: ../project/GenericTCPServer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o   ../project/GenericTCPServer.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/ARP.o: ../project/ARP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ARP.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ARP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/ARP.o   ../project/ARP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/ARP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/ARP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/DNS.o: ../project/DNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/DNS.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/DNS.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/DNS.o   ../project/DNS.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/DNS.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/DNS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/ETH97J60.o: ../project/ETH97J60.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ETH97J60.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ETH97J60.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/ETH97J60.o   ../project/ETH97J60.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/ETH97J60.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/ETH97J60.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/Helpers.o: ../project/Helpers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Helpers.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Helpers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Helpers.o   ../project/Helpers.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Helpers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Helpers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/ICMP.o: ../project/ICMP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ICMP.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ICMP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/ICMP.o   ../project/ICMP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/ICMP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/ICMP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/IP.o: ../project/IP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/IP.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/IP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/IP.o   ../project/IP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/IP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/IP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/StackTsk.o: ../project/StackTsk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/StackTsk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/StackTsk.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/StackTsk.o   ../project/StackTsk.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/StackTsk.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/StackTsk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/TCP.o: ../project/TCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/TCP.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/TCP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/TCP.o   ../project/TCP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/TCP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/TCP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/Tick.o: ../project/Tick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Tick.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Tick.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Tick.o   ../project/Tick.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Tick.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Tick.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/UDP.o: ../project/UDP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/UDP.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/UDP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/UDP.o   ../project/UDP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/UDP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/UDP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/main.o: ../project/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/main.o   ../project/main.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
else
${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o: ../project/GenericTCPClient.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o   ../project/GenericTCPClient.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/GenericTCPClient.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o: ../project/GenericTCPServer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o   ../project/GenericTCPServer.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/GenericTCPServer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/ARP.o: ../project/ARP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ARP.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ARP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/ARP.o   ../project/ARP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/ARP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/ARP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/DNS.o: ../project/DNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/DNS.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/DNS.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/DNS.o   ../project/DNS.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/DNS.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/DNS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/ETH97J60.o: ../project/ETH97J60.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ETH97J60.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ETH97J60.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/ETH97J60.o   ../project/ETH97J60.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/ETH97J60.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/ETH97J60.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/Helpers.o: ../project/Helpers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Helpers.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Helpers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Helpers.o   ../project/Helpers.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Helpers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Helpers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/ICMP.o: ../project/ICMP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ICMP.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/ICMP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/ICMP.o   ../project/ICMP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/ICMP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/ICMP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/IP.o: ../project/IP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/IP.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/IP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/IP.o   ../project/IP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/IP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/IP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/StackTsk.o: ../project/StackTsk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/StackTsk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/StackTsk.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/StackTsk.o   ../project/StackTsk.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/StackTsk.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/StackTsk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/TCP.o: ../project/TCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/TCP.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/TCP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/TCP.o   ../project/TCP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/TCP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/TCP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/Tick.o: ../project/Tick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Tick.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/Tick.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/Tick.o   ../project/Tick.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/Tick.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/Tick.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/UDP.o: ../project/UDP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/UDP.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/UDP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/UDP.o   ../project/UDP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/UDP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/UDP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1989816886/main.o: ../project/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/main.o   ../project/main.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteIO.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../project/18f97j60_g.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\project\18f97j60_g.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w -x -u_DEBUG -m"$(BINDIR_)$(TARGETBASE).map" -w -l"../../../CMS2 23032021 - Reims jaures/carte com IP MPLAB/carte com IP MPLAB/TCPIP/Demo App" -l"."  -z__MPLAB_BUILD=1  -u_CRUNTIME -z__MPLAB_DEBUG=1 -z__MPLAB_DEBUGGER_ICD3=1 $(MP_LINKER_DEBUG_OPTION) -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteIO.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
else
dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteIO.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../project/18f97j60_g.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\project\18f97j60_g.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w  -m"$(BINDIR_)$(TARGETBASE).map" -w -l"../../../CMS2 23032021 - Reims jaures/carte com IP MPLAB/carte com IP MPLAB/TCPIP/Demo App" -l"."  -z__MPLAB_BUILD=1  -u_CRUNTIME -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteIO.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif

/*********************************************************************
 *
 *                  Headers for TCPIP Demo App
 *
 *********************************************************************
 * FileName:        MainDemo.h
 * Dependencies:    Compiler.h
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.05 or higher
 *					Microchip C30 v3.12 or higher
 *					Microchip C18 v3.30 or higher
 *					HI-TECH PICC-18 PRO 9.63PL2 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2010 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *		ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *		used in conjunction with a Microchip ethernet controller for
 *		the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 *
 * Author               Date    Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * E. Wood				4/26/08 Copied from MainDemo.c
 ********************************************************************/
#ifndef _MAINDEMO_H
#define _MAINDEMO_H

#define BAUD_RATE       (19200)		// bps

#if !defined(THIS_IS_STACK_APPLICATION)
	extern BYTE AN0String[8];
#endif

void DoUARTConfig(void);

#if defined(EEPROM_CS_TRIS) || defined(SPIFLASH_CS_TRIS)
	void SaveAppConfig(const APP_CONFIG *AppConfig);
#else
	#define SaveAppConfig(a)
#endif

void SMTPDemo(void);
void PingDemo(void);
void SNMPTrapDemo(void);
void SNMPV2TrapDemo(void);
void GenericTCPClient(void);
void GenericTCPServer(void);
void BerkeleyTCPClientDemo(void);
void BerkeleyTCPServerDemo(void);
void BerkeleyUDPClientDemo(void);







// Define a header structure for validating the AppConfig data structure in EEPROM/Flash
typedef struct
{
	unsigned short wConfigurationLength;	// Number of bytes saved in EEPROM/Flash (sizeof(APP_CONFIG))
	unsigned short wOriginalChecksum;		// Checksum of the original AppConfig defaults as loaded from ROM (to detect when to wipe the EEPROM/Flash record of AppConfig due to a stack change, such as when switching from Ethernet to Wi-Fi)
	unsigned short wCurrentChecksum;		// Checksum of the current EEPROM/Flash data.  This protects against using corrupt values if power failure occurs while writing them and helps detect coding errors in which some other task writes to the EEPROM in the AppConfig area.
} NVM_VALIDATION_STRUCT;


// An actual function defined in MainDemo.c for displaying the current IP 
// address on the UART and/or LCD.
void DisplayIPValue(IP_ADDR IPVal);

  
//IO PROGRAMME
void IntToChar(unsigned int value, char *chaine,char Precision);
unsigned int CRC16(char far *txt, unsigned  int lg_txt);
void delay_qms(char tempo);
unsigned char LIRE_EEPROM(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char r);
unsigned char WriteI2CC( unsigned char data_out ); //COPIE
unsigned char getsI2CC( unsigned char *rdptr, unsigned char length ); //COPIE 
void Init_RS485(void);
void Init_I2C(void);
void ENVOYER_GROUPE (void);
void TRAMECAPTEURBIDON(char *taz);
char RECEPTTCPACK (void);
void voir_qui_existe (char ES) ;
void TRANSFORMER_TRAME_MEMOIRE_EN_TELGAT(char c,char n); // que faire,numero cod
unsigned char choix_memoire(unsigned char *tab);
unsigned int calcul_addmemoire (unsigned char *tab);
char MOUCHARD2(void);
char MOUCHARD(char carte);
void TEMPO(char seconde);
void gestion_menu8 (void);
void gestion_menu6b (void);
void gestion_menu6a (void);
void envoi_cursur (void);
void gestion_menu6 (void);
void position_init (char Npage, char Nchamps);
void TRAMERECEPTIONminitel (void);
void gestion_menu5 (void);
void gestion_menu3 (void);
void gestion_menu2 (void);
void gestion_menu1 (void);
void TRAMERECEPTIONTCPDONNEES (int val2); 
void nom_site ( void);
void gestion_page (void);
void TRAMEENVOITCPDONNEES (int val);
void LIRE_HEURE_MINITEL(void);
void Recuperer_param_cms(void);
void enregistrer_donnees (void);
char TYPEDECARTE(void);
void CHERCHER_DONNNEES_CABLES(char *g,char j); //051 cable et codage j //DONNE LES DONNEES DU CABLE ET DU CODAGE CHOISSI ET MET UNE VARIABLE EXISTENCE A UN SI CAPTEUR EXISTE
void calcul_c_l (void); 
char gestion_rob(void);
void tabulation (void);
 void gauche (void);
void droite (void);
void saisie (void);
unsigned char resume_voies(char y,char deb); //ANALYSE L'ETAT VOIES EXIST. y numero carte et deb mettre dans TRAMEOUT A PARTIR DE LA pour menu 2 alarme
char RECEVOIR_TCP(void);
void variables_groupe(char t);
char CHERCHER_DONNEES_GROUPE(void);
void analyser_les_alarmes_en_cours(void); //ANALYSE LES CABLES ET LES ETATS POPUR L'INSTANT
void haut (void);
char VALIDER_CAPTEUR(void);
void sup_capteur (void);   /////
void creer_trame_horloge (void);  //CREATION D'UNR TRAME AVEC LES DONNEES DE L'HORLOGE JJMMsshhmmAA
void infos_etats(char *y,char t); //ADDITIONNE LE TYPE D'ETAT
unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char g);
void MODIFICATION_TABLE_DEXISTENCE(char *v,char eff,char etat); //eff pour effacer (eff=2 alors recopie) alors et etat ligne pour fusible HS ou CC ou surtension 
void TRAMERECEPTIONRS485DONNEES (char tout);
void ENVOIE_DES_TRAMES (char R,int fonctionRS,char *tab); 
void CREER_TRAMEIN_CAPTEUR_DU_MINITEL(void); //CREER LA TRAME QUE L'ON VA SAV DANS LEEPROM
char EFFACER_CAPTEUR(void);
unsigned char LIRE_HORLOGE(unsigned char zone); //LIRE LES DONNEES HORLOGE AUX CHOIX
unsigned char putstringI2C(unsigned char *wrptr);
char RECEVOIR_RS485(char ttout);
void TRANSFORMER_TRAME_MEM_EN_COM (void);
void recuperer_trame_memoire (char w); //RECUPERE TRAME MET DANS LES VARIABLES PUIS ECRIT DANS L'EPROOM
void SAV_EXISTENCE(char *tt);
void entrer_trame_horloge (void); //PREPARE TRAME POUR ECRIRE L'HORLOGE EN FORMAT 
void GROUPE_EN_MEMOIRE(unsigned char *tab,char lui); 
void controle_CRC(void);
void TRAMEENVOIRS485DONNEES (void); 
void calcul_CRC(void);
signed char litHORLOGE_i2c(unsigned char NumeroH,unsigned char AddH); 
void RS485(unsigned char carac);
unsigned char ECRIRE_HORLOGE(unsigned char zone,unsigned char Time);
unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH,unsigned char ADDH, unsigned char dataH0);
void testINT2 (void);
void init_RS232(void);
void delay_qmsip(char tempIP);
 void timerTCPIP (void);
void ecouteTCPIP(void);
void INITGESTIONCARTEIO (void);
void mainio(void);
void MOUCHARDREGLAHEURE(void);
void SAV_CHG_ALARME(unsigned char *tab); 
unsigned char choix_memoirePRECEDENT(unsigned char *tab);
unsigned int calcul_addmemoirePRECEDENT(unsigned char *tab);
void ANALYSE_SE_ALARME (void);
void client (void);
void controle_CRC2021(void);
void IntToChar5(unsigned int value, char *chaine, char Precision);
#endif // _MAINDEMO_H
